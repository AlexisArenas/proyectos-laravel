@extends('layout.main')

@section('titulo')
    <title>Menu | Cliente</title>
@endsection

@section('css')

@endsection

@section('titulo-pagina')
    <h1 class="h3 mb-4 text-gray-800">Bienvenido cliente: {{session('usuario')->Nombre}}</h1>
    <img src="https://images.unsplash.com/photo-1537569894557-62db9d9bf404?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Nnx8YW1lcmljYW4lMjBmb290YmFsbHxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" alt="">
    @if(isset($estatus))
        <h6 class="m-0 font-weight-bold text-primary">GRACIAS, POR TU COMPRA</h6>
    @endif
@endsection

@section('contenido')

@endsection

@section('js')

@endsection

