@extends('layout.mainAdmin')

@section('titulo')
    <title>Pedidos | Admin</title>
@endsection

@section('css')
    <link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('titulo-pagina')
    <h1 class="h3 mb-4 text-gray-800">Pedidos</h1>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Articulos pedidos en tienda</h6>
            </div>
            <div class="card-body">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">A continuación se muestra todos los articulos comprados.</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID_Pedido</th>
                                    <th>ID_Cliente</th>
                                    <th>ID_Producto</th>
                                    <th>Cantidad</th>
                                    <th>Costo Total</th>
                                    <th>Fecha</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($pedidos as $pedido)
                                    <tr>
                                        <td>{{$loop->index + 1}}</td>
                                        <td>{{$pedido->ID_Carrito}}</td>
                                        <td>{{$pedido->ID_Cliente}}</td>
                                        <td>{{$pedido->ID_Producto}}</td>
                                        <td>{{$pedido->Cantidad}}</td>
                                        <td>{{$pedido->Costo_Total}}</td>
                                        <td>{{$pedido->created_at}}</td>

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <!-- Page level plugins -->
    <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function (){
            $('#dataTable').DataTable();
        });
    </script>
@endsection

