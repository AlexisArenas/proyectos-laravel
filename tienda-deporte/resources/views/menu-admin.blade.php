@extends('layout.mainAdmin')

@section('titulo')
    <title>Menu|Admin</title>
@endsection

@section('css')

@endsection

@section('titulo-pagina')
    <h1 class="h3 mb-4 text-gray-800">Bienvenido administrador: {{session('usuario')->Nombre}}</h1>
    <h3>Aqui podras realizar el registro de nuevos productos para la tienda, consultar pedidos o ver los productos disponibles</h3>
    <br>
    <img src="https://images.unsplash.com/photo-1600577916048-804c9191e36c?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=70" alt="">
@endsection

@section('contenido')

@endsection

@section('js')

@endsection

