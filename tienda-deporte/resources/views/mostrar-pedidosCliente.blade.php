@extends('layout.main')

@section('titulo')
    <title>Mis pedidos | Cliente</title>
@endsection

@section('css')
    <link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('titulo-pagina')
    <h1 class="h3 mb-4 text-gray-800">Pedidos</h1>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Pedidos:</h6>
            </div>
            <div class="card-body">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">A continuación se muestra información sobre los productos que has comprado</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID_Producto</th>

                                    <th>Nombre</th>
                                    <th>Cantidad</th>
                                    <th>Costo Total</th>

                                </tr>
                                <tbody>
                                @foreach($carritos as $car)
                                    <tr>
                                        <td>{{$loop->index + 1}}</td>
                                        <td>{{$car->ID_Producto}}</td>

                                        <td>{{$car->NombreP}}</td>
                                        <td>{{$car->Cantidad}}</td>
                                        <td>{{$car->Costo_Total}}</td>

                                        <td><button class="bg-gradient-info form-control text-white" id="cancelar-pedido">Cancelar</button></td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <!-- Page level plugins -->
    <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script>

        $(document).ready(function (){
            $('#dataTable').DataTable();

            /// -- Ajax
            $("#cancelar-pedido").click(function (e){
                e.preventDefault();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "get",
                    url: "{{route('eliminar.pedidos',['id'=> $car->ID_Producto])}}",
                    dataType: 'json',
                    cache: false,
                    success: function (data) {

                    }
                });

            });
        });

    </script>
@endsection

