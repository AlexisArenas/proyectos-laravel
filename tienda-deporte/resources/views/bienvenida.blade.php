<!DOCTYPE HTML>
<!--
	Aerial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<head>
    <title>Bienvenido a la tienda de deportes</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="/css/carga/main.css" />
    <noscript><link rel="stylesheet" href="/css/carga/noscript.css" /></noscript>
    <style type="text/css">
        .boton_personalizado{
            text-decoration: none;
            padding: 10px;
            font-weight: 600;
            font-size: 20px;
            color: #ffffff;
            background-color: #b70322;
            border-radius: 6px;
            border: 2px solid #f1f1f1;

        }
    </style>
</head>
<body class="is-preload">
<div id="wrapper">
    <div id="bg"></div>
    <div id="overlay"></div>
    <div id="main">

        <!-- Header -->
        <header id="header">
            <h1>Bienvenido a la tienda de deportes</h1>
            <h1> "Buena Onda"</h1>
            <br>
            <p style="margin-bottom: 5%;">Compra&nbsp;&bull;&nbsp; Consulta &nbsp;&bull;&nbsp; Pagina de prueba</p>
            <a href="{{route('login.cliente')}}" class="boton_personalizado" href="#">¡Entrar como Cliente!</a>
            <a href="{{route('login.admin')}}" class="boton_personalizado" href="#">¡Entrar como Administrador!</a>
            <nav style="margin-top: 5%;">
                <ul>
                    <li><a href="#" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="#" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
                    <li><a href="#" class="icon brands fa-dribbble"><span class="label">Dribbble</span></a></li>
                    <li><a href="#" class="icon brands fa-github"><span class="label">Github</span></a></li>
                    <li><a href="#" class="icon solid fa-envelope"><span class="label">Email</span></a></li>
                </ul>
            </nav>
        </header>

        <!-- Footer -->
        <footer id="footer">
            <span class="copyright">&copy; Untitled. Design: <a href="#">Axel Adan, Alejandro Suarez y Alexis Arenas</a>.</span>
        </footer>

    </div>
</div>
<script>
    window.onload = function() { document.body.classList.remove('is-preload'); }
    window.ontouchmove = function() { return false; }
    window.onorientationchange = function() { document.body.scrollTop = 0; }
</script>
</body>
</html>
