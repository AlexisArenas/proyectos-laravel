@extends('layout.main')

@section('titulo')
    <title>Productos | Cliente</title>
@endsection

@section('css')
    <link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('titulo-pagina')
    <h1 class="h3 mb-4 text-gray-800">Productos</h1>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Productos:</h6>
            </div>
            <div class="card-body">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">A continuación se muestra información sobre los productos que tiene la tienda</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Cantidad Disponible</th>
                                    <th>Precio</th>
                                    <th>Descripcion</th>
                                    <th>Tipo</th>
                                    <th>Imagen 1</th>
                                    <th>Imagen 2</th>

                                </tr>

                                <tbody>
                                @foreach($producto as $product)
                                    <tr>
                                        <td>{{$loop->index + 1}}</td>
                                        <td {{$product->ID_Producto}} </td>
                                        <td>{{$product->Nombre}}</td>
                                        <td>{{$product->Cantidad_Disponible}}</td>
                                        <td>{{$product->Precio_Venta}}</td>
                                        <td>{{$product->Descripcion}}</td>
                                        <td>{{$product->Tipo}}</td>
                                        <td><img src="{{$product->Imagen1}}" alt="" width="100"></td>
                                        <td><img src="{{$product->Imagen2}}" alt="" width="100"></td>

                                        <td><a href="{{route('agregar.carrito.producto',['id' => $product->ID_Producto])}}" class="btn-info">+Agregar</a></td>

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <!-- Page level plugins -->
    <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function (){
            $('#dataTable').DataTable();

        });

    </script>
@endsection

