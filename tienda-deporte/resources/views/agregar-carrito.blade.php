@extends('layout.main')

@section('titulo')
    <title>Agregar | Carrito</title>
@endsection

@section('css')

@endsection

@section('titulo-pagina')
    <h1 class="h3 mb-4 text-gray-800">Agregar al carrito</h1>
    @if(isset($estatus))
        @if($estatus == "success")
            <label class="text-success">{{$mensaje}}</label>
        @elseif($estatus == "error")
            <label class="text-warning">{{$mensaje}}</label>
        @endif
    @endif

@endsection

@section('contenido')
    <form class="user" method="post" action="">
        {{csrf_field()}}
        <div class="form-group row">
            <div class="col-sm-6">
                <label for="">ID del producto</label>
                <input type="text" class="form-control form-control-user"
                       id="idP" placeholder="ID del producto" name="idP" value="{{$id}}" disabled>
            </div>

            <div class="col-sm-6">
                <label for="">Nombre del producto</label>

                <input type="text" class="form-control form-control-user"
                       id="nombre" placeholder="Nombre del producto" value="{{$nombre}}" name="nombre" disabled>
            </div>

            <div class="col-sm-6">
                <label for="">Cantidad</label>

                <input type="text" class="form-control form-control-user" value="{{$cantidad}}"
                       id="cantidad" placeholder="" name="cantidad" disabled>


            </div>
            <div class="col-sm-6">
                <label for="">Costo total</label>

                <input type="text" class="form-control form-control-user"
                       id="costo" placeholder="Costo Total" name="costo" disabled value="{{$costo}}">
            </div>
            <div class="col-sm-6 row">
                <div class="col-sm-3">
                    <br>
                    <a class="form-control btn-success" id="agregar" href="{{route('agregar.carrito.producto.resta',['id'=> $id, 'cantidad'=>$cantidad])}}">-</a>

                </div>
                <div class="col-sm-3">
                    <br>
                    <a class="form-control btn-success" id="quitar" href="{{route('agregar.carrito.producto.suma',['id'=> $id, 'cantidad'=>$cantidad])}}">+</a>

                </div>
            </div>
        </div>

        <a class="form-control btn-primary" id="enviar" href="{{route('registrar.compra.form',['id'=> $id, 'cantidad'=>$cantidad, 'costo'=>$costo])}}">Comprar</a>
    </form>
@endsection

@section('js')
    <script>

    </script>
@endsection

