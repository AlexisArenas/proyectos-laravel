@extends('layout.mainAdmin')

@section('titulo')
    <title>Registro|Admin</title>
@endsection

@section('css')

@endsection

@section('titulo-pagina')
    <h1 class="h3 mb-4 text-gray-800">Agregar Productos</h1>
    @if(isset($estatus))
        @if($estatus == "success")
            <label class="text-success">{{$mensaje}}</label>
        @elseif($estatus == "error")
            <label class="text-warning">{{$mensaje}}</label>
        @endif
    @endif

@endsection

@section('contenido')
    <form class="user" method="post" action="{{route('registro.producto.form')}} " enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group row">
            <div class="col-sm-4">
                <input type="text" class="form-control form-control-user"
                        id="nombre" placeholder="Nombre del producto" name="nombre" required>
            </div>
            <div class="col-sm-4">
                <input type="text" class="form-control form-control-user"
                        id="cantidadI" placeholder="Cantidad Inicial" name="cantidadI"required>
            </div>
            <div class="col-sm-4">
                <input type="text" class="form-control form-control-user"
                        id="precioC" placeholder="Precio Compra" name="precioC"required>
            </div>

        </div>
        <div class="form-group row">
            <div class="col-sm-4">
                <input type="text" class="form-control form-control-user"
                        id="precioV" placeholder="Precio Venta" name="precioV" required>
            </div>
            <div class="col-sm-4">
                <input type="text" class="form-control form-control-user"
                        id="desc" placeholder="Descipcion" name="desc"required>
            </div>
            <div class="col-sm-4">
                <input type="text" class="form-control form-control-user"
                        id="tipo" placeholder="Tipo" name="tipo"required>
            </div>

        </div>
        <div class="form-group row">
            <div class="col-sm-6">
                <label for="imageOne">Suba una una imagen</label>

                <input type="file" class="form-control form-control-user"
                       id="imageOne" name="imageOne" required>
            </div>
            <div class="col-sm-6">
                <label for="imageTwo">Suba una otra imagen</label>
                <input type="file" class="form-control form-control-user"
                       id="imageTwo" name="imageTwo"required>
            </div>


        </div>

        <hr>
        <input type="submit" name="enviar" class="btn btn-primary btn-user btn-block" value="Registrar">
    </form>
@endsection

@section('js')

@endsection

