<?php

namespace App\Http\Controllers;

use App\Models\Administradores;
use App\Models\Clientes;
use App\Models\Carrito;
use App\Models\Productos;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;


class CarritoController extends Controller
{
    public function agregarCarrito(){
        return view("agregar-carrito");
    }
    public function misPedidos(){
        return view("mostrar-pedidosCliente");
    }
    public function agregarP($idp, $cantidadPedida){

        $cantidadP = Productos::where('ID_Producto',$idp)->first();
        $cantidadMax = $cantidadP->Cantidad_Disponible;

        if($cantidadPedida>$cantidadMax){
            return view("agregar-carrito",["estatus"=> "error","mensaje"=> "No contamos con la cantidad suficiente"]);
        }

        $cantidadPedida = $cantidadPedida+1;
        $costo = ($cantidadP->Precio_Venta)*$cantidadPedida;
        return view("agregar-carrito",["estatus"=> "succes", "id"=> $idp, "costo"=> $costo, "nombre"=>$cantidadP->Nombre, "cantidad"=>$cantidadPedida]);
    }
    public function quitarP($idp, $cantidadPedida){

        $cantidadP = Productos::where('ID_Producto',$idp)->first();

        $cantidadPedida = $cantidadPedida-1;
        $costo = ($cantidadP->Precio_Venta)*$cantidadPedida;
        if($cantidadPedida<1){
            $costo = ($cantidadP->Precio_Venta)*1;

            return view("agregar-carrito",["estatus"=> "error","mensaje"=> "No puedes pedir 0","id"=> $idp, "costo"=> $cantidadP->Precio_Venta, "nombre"=>$cantidadP->Nombre, "cantidad"=>1]);
        }
        return view("agregar-carrito",["estatus"=> "succes", "id"=> $idp, "costo"=> $costo, "nombre"=>$cantidadP->Nombre, "cantidad"=>$cantidadPedida]);
    }
    public function agregar($idP)
    {
        $producto = Productos::where('ID_Producto', $idP)->first();
        $costo = $producto->Precio_Venta;
        $nombre = $producto->Nombre;
        $cantidad = 1;

        return view("agregar-carrito",["estatus"=> "succes", "id"=> $idP, "costo"=> $costo, "nombre"=>$nombre, "cantidad"=> $cantidad]);

    }

    public function regisrtarCompra($idp, $cantidadPedida, $costo){

        $carrito = new Carrito();

        $idCliente = session('usuario')->ID_Cliente;;
        $idproducto= $idp;
        $carrito->ID_Cliente = $idCliente;
        $carrito->ID_Producto = $idp;
        $carrito->Cantidad = $cantidadPedida;
        $carrito->Costo_Total = $costo;
        $carrito->Estatus = "Pedido";
        $carrito->save();

        return view("menu-cliente",["estatus"=> "succes"]);
    }

    public function mostrarPedidos(){
        $carrito  = Carrito::where('ID_Cliente',session('usuario')->ID_Cliente)->get();

        foreach ($carrito as $elemento){
            $producto = Productos::where('ID_Producto',$elemento->ID_Producto)->first();
            $elemento->NombreP = $producto->Nombre;
        }
        return view("mostrar-pedidosCliente",["carritos"=> $carrito]);

    }
    public function mostrarPedidosTienda(){
        $pedido  = Carrito::get();

        foreach ($pedido as $elemento){
            $producto = Productos::where('ID_Producto',$elemento->ID_Producto)->first();
            $elemento->NombreP = $producto->Nombre;
        }
        return view("pedidos-tienda",["pedidos"=> $pedido]);

    }
    public function eliminarPedido ($id){
        $pedido = Carrito::find($id);
        $pedido->delete();
        return redirect()->route('mis.pedidos');
    }


}
