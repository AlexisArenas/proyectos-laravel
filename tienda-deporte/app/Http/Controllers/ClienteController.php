<?php

namespace App\Http\Controllers;

use App\Models\Administradores;
use App\Models\Clientes;
use App\Models\Carrito;
use App\Models\Productos;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class ClienteController extends Controller
{

    public function registro(){
        return view("registroCliente");
    }
    public function login(){
        return view("login-cliente");
    }

    public function menu(){
        return view("menu-cliente");
    }

    public function registroForm(Request $datos){

        if(!$datos->correo || !$datos->password1 || !$datos->password2 || !$datos->nombre || !$datos->apPaterno || !$datos->apMaterno)
        return view("registroCliente",["estatus"=> "error", "mensaje"=> "¡Falta información!"]);

        $cliente = Clientes::where('correo',$datos->correo)->first();
        if($cliente)
            return view("registroCliente",["estatus"=> "error", "mensaje"=> "¡El correo ya se encuentra registrado!"]);

        $nombre = $datos->nombre;
        $apPaterno = $datos->apPaterno;
        $apMaterno = $datos->apMaterno;
        $correo = $datos->correo;
        $password2 = $datos->password2;
        $password1 = $datos->password1;

        if($password1 != $password2)
            return view("registroCliente",["estatus" => "error", "mensaje"=>"¡Las contraseñas son diferentes!"]);
        

        $cliente = new Clientes();
        $cliente->Nombre = $nombre;
        $cliente->Ap_Paterno = $apPaterno;
        $cliente->Ap_Materno = $apMaterno;
        $cliente->Correo =  $correo;
        $cliente->Contrasenia = bcrypt($password1);
        $cliente->save();

        return view("login-cliente",["estatus"=> "success", "mensaje"=> "¡Cuenta Creada!"]);

    }
    
    public function verificarCredenciales(Request $datos){

        if(!$datos->correo || !$datos->password)
            return view("login-cliente",["estatus"=> "error", "mensaje"=> "¡Completa los campos!"]);

        $cliente = Clientes::where("Correo",$datos->correo)->first();
        if(!$cliente)
            return view("login-cliente",["estatus"=> "error", "mensaje"=> "¡El correo no esta registrado!"]);

        if(!Hash::check($datos->password,$cliente->Contrasenia))
            return view("login-cliente",["estatus"=> "error", "mensaje"=> "¡Datos incorrectos!"]);

        Session::put('usuario',$cliente);

        if(isset($datos->url)){
            $url = decrypt($datos->url);
            return redirect($url);
        }else{
            return redirect()->route('cliente.menu');
        }

    }

    public function cerrarSesion(){
        if(Session::has('usuario'))
            Session::forget('usuario');

        return redirect()->route('login.cliente');
    }
}
