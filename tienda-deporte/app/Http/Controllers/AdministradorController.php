<?php

namespace App\Http\Controllers;
use App\Models\Administradores;
use App\Models\Clientes;
use App\Models\Carrito;
use App\Models\Productos;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdministradorController extends Controller
{
    public function bienvenida(){
        return view("bienvenida");
    }
    public function login(){
        return view("login-admin");
    }
    public function registro(){
        return view("registroAdmin");
    }

    public function menu(){
        return view("menu-admin");
    }

    public function registroForm(Request $datos){

        if(!$datos->correo || !$datos->password1 || !$datos->password2 || !$datos->nombre || !$datos->apPaterno || !$datos->apMaterno)
            return view("registroAdmin",["estatus"=> "error", "mensaje"=> "¡Falta información!"]);

        $admin = Administradores::where('correo',$datos->correo)->first();
        if($admin)
            return view("registroAdmin",["estatus"=> "error", "mensaje"=> "¡El correo ya se encuentra registrado!"]);

        $nombre = $datos->nombre;
        $apPaterno = $datos->apPaterno;
        $apMaterno = $datos->apMaterno;
        $correo = $datos->correo;
        $password2 = $datos->password2;
        $password1 = $datos->password1;

        if($password1 != $password2){
            return view("registroAdmin",["estatus" => "error", "mensaje"=>"¡Las contraseñas son diferentes!"]);
        }

        $admin = new Administradores();
        $admin->Nombre = $nombre;
        $admin->Ap_Paterno = $apPaterno;
        $admin->Ap_Materno = $apMaterno;
        $admin->Correo =  $correo;
        $admin->Contrasenia = bcrypt($password1);
        $admin->save();

        return view("login-admin",["estatus"=> "success", "mensaje"=> "¡Cuenta Creada!"]);

    }

    public function verificarCredenciales(Request $datos){

        if(!$datos->correo || !$datos->password)
            return view("login-admin",["estatus"=> "error", "mensaje"=> "¡Completa los campos!"]);

        $admin = Administradores::where("Correo",$datos->correo)->first();

        if(!$admin)
            return view("login-admin",["estatus"=> "error", "mensaje"=> "¡El correo no esta registrado!"]);

        if(!Hash::check($datos->password,$admin->Contrasenia))
            return view("login-admin",["estatus"=> "error", "mensaje"=> "¡Datos incorrectos!"]);

        Session::put('usuario',$admin);

        if(isset($datos->url)){
            $url = decrypt($datos->url);
            return redirect($url);
        }else{
            return redirect()->route('admin.menu');
        }


    }

    public function cerrarSesion(){
        if(Session::has('admin'))
            Session::forget('admin');

        return redirect()->route('login.admin');
    }



}
