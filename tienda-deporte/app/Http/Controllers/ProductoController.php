<?php

namespace App\Http\Controllers;

use App\Models\Administradores;
use App\Models\Clientes;
use App\Models\Carrito;
use App\Models\Productos;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class ProductoController extends Controller
{
    public function registro(){
        return view("registroProducto");
    }
    public function mostrar(){
        return view("mostrar-productos");
    }
    public function mostrarCliente(){
        return view("mostrar-productosCliente");
    }

    public function registroForm(Request $datos){

        if(!$datos->nombre || !$datos->cantidadI || !$datos->precioC || !$datos->precioV || !$datos->desc || !$datos->tipo || !$datos->hasFile('imageOne') || !$datos->hasFile('imageTwo'))
            return view("registroProducto",["estatus"=> "error", "mensaje"=> "¡Falta información!"]);


        $producto = Productos::where('Nombre',$datos->nombre)->first();
        if($producto)
            return view("registroProducto",["estatus"=> "error", "mensaje"=> "¡El producto ya se encuentra registrado!"]);



        $nombre = $datos->nombre;
        $cantidadI = $datos->cantidadI;
        $cantidadD = $cantidadI;
        $precioC = $datos->precioC;
        $precioV = $datos->precioV;
        $desc = $datos->desc;
        $tipo = $datos->tipo;
        $idAdmin = session('usuario')->ID_Admin;

        $fileOne = $datos->file("imageOne");
        $fileTwo = $datos->file("imageTwo");

        $nameOne = $fileOne->getClientOriginalName();
        $nameTwo = $fileTwo->getClientOriginalName();

        $extensionOne = $fileOne->getClientOriginalExtension();
        $extensionTwo = $fileTwo->getClientOriginalExtension();

        if($extensionOne != "png" && $extensionOne != "jpg" && $extensionOne != "PNG"  && $extensionOne != "JPG"  && $extensionOne != "jpeg"  && $extensionOne != "JPEG"){
            return view("registroProducto",["estatus"=> "error", "mensaje"=> "¡Formato no permitido!"]);

        }
        elseif($extensionTwo != "png" && $extensionTwo != "jpg" && $extensionTwo != "PNG"  && $extensionTwo != "JPG"  && $extensionTwo != "jpeg"  && $extensionTwo != "JPEG"){
            return view("registroProducto",["estatus"=> "error", "mensaje"=> "¡Formato no permitido!"]);
        }

        $fileOne->move(public_path().'/images/', $nameOne);
        $fileTwo->move(public_path().'/images/', $nameTwo);
        $ruta = '/images/';

        $producto = new Productos();
        $producto->Nombre = $nombre;
        $producto->Cantidad_Inicial = $cantidadI;
        $producto->Cantidad_Disponible = $cantidadD;
        $producto->Precio_Compra =  $precioC;
        $producto->Precio_Venta = $precioV;
        $producto->Descripcion = $desc;
        $producto->Tipo = $tipo;
        $producto->Imagen1 = $ruta.$nameOne;
        $producto->Imagen2 = $ruta.$nameTwo;
        $producto->ID_AdminAutor = $idAdmin;

        $producto->save();

        return view("registroProducto",["estatus"=> "success", "mensaje"=> "¡Producto Registrado!"]);

    }
    public function mostrarTodo(){
        $producto = Productos::get();
        return view("mostrar-productos",["producto" => $producto]);

    }
    public function mostrarTodoCliente(){
        $producto = Productos::get();
        return view("mostrar-productosCliente",["producto" => $producto]);

    }
   

}
