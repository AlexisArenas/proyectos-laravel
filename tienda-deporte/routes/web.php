<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdministradorController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\CarritoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('bienvenida');
});

Route::get('/bienvenida',[AdministradorController::class,'bienvenida'])->name('bienvenida');

Route::get('/login/admin',[AdministradorController::class,'login'])->name('login.admin');
Route::post('/login/admin',[AdministradorController::class,'verificarCredenciales'])->name('login.form.admin');
Route::get('/registro/admin',[AdministradorController::class,'registro'])->name('registro.admin');
Route::post('/registro/admin',[AdministradorController::class,'registroForm'])->name('registro.form.admin');

Route::get('/login/cliente',[ClienteController::class,'login'])->name('login.cliente');
Route::post('/login/cliente',[ClienteController::class,'verificarCredenciales'])->name('login.form.cliente');
Route::get('/registro/cliente',[ClienteController::class,'registro'])->name('registro.cliente');
Route::post('/registro/cliente',[ClienteController::class,'registroForm'])->name('registro.form.cliente');

Route::get('/cerrarSesion/admin',[AdministradorController::class,'cerrarSesion'])->name('cerrar.sesion.admin');
Route::get('/cerrarSesion/cliente',[ClienteController::class,'cerrarSesion'])->name('cerrar.sesion.cliente');


Route::prefix('/admin')->middleware("VerificarUsuario")->group(function (){
    Route::get('/menu',[AdministradorController::class,'menu'])->name('admin.menu');
    Route::get('/registro/producto',[ProductoController::class,'registro'])->name('registro.producto');
    Route::post('/registro/producto',[ProductoController::class,'registroForm'])->name('registro.producto.form');
    Route::get('/mostrar/producto',[ProductoController::class,'mostrarTodo'])->name('mostrar.producto');
    Route::get('/pedidos/realizados',[CarritoController::class,'mostrarPedidosTienda'])->name('pedidos.tienda');

});
Route::prefix('/usuario')->middleware("VerificarUsuario")->group(function (){
    Route::get('/menu',[ClienteController::class,'menu'])->name('cliente.menu');
    Route::get('/mostrar/productos',[ProductoController::class,'mostrarTodoCliente'])->name('mostrar.producto.cliente');
    Route::get('/agregar/carrito',[CarritoController::class,'agregarCarrito'])->name('agregar.carrito');
    Route::get('/agregar/carrito/{id}',[CarritoController::class,'agregar'])->name('agregar.carrito.producto');
    Route::get('/agregar/carrito/{id}/{cantidad}',[CarritoController::class,'agregarP'])->name('agregar.carrito.producto.suma');
    Route::get('/quitar/carrito/{id}/{cantidad}',[CarritoController::class,'quitarP'])->name('agregar.carrito.producto.resta');
    Route::get('/registrar/compra/form/{id}/{cantidad}/{costo}',[CarritoController::class,'regisrtarCompra'])->name('registrar.compra.form');
    Route::get('/mis/compras',[CarritoController::class,'misPedidos'])->name('mis.compras');
    Route::get('/mis/pedidos/realizados',[CarritoController::class,'mostrarPedidos'])->name('mis.pedidos');
    Route::get('/eliminar/pedidos/realizados/{id}',[CarritoController::class,'eliminarPedido'])->name('eliminar.pedidos');



});
