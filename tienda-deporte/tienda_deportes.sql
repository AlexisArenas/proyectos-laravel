-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-04-2021 a las 04:26:06
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda_deportes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administradores`
--

CREATE TABLE `administradores` (
  `ID_Admin` int(11) NOT NULL,
  `Nombre` varchar(25) NOT NULL,
  `Ap_Paterno` varchar(25) NOT NULL,
  `Ap_Materno` varchar(25) NOT NULL,
  `Correo` varchar(30) NOT NULL,
  `Contrasenia` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administradores`
--

INSERT INTO `administradores` (`ID_Admin`, `Nombre`, `Ap_Paterno`, `Ap_Materno`, `Correo`, `Contrasenia`, `updated_at`, `created_at`) VALUES
(2, 'Alexis', 'Arenas', 'Joaquin', 'alexis@hotmail.com', '$2y$10$dG9P8KQvtSmJxFLe1rlE9eLk5vWc4ghWSBrLp7Fs24Fv/B8EjmAHK', '2021-03-31 11:02:21', '2021-03-31 11:02:21'),
(3, 'Lombardo', 'Mauro', 'Ezequiel', 'hola@hotmial.com', '$2y$10$Hy/b0fBxXsWpQdfs.EusXO2e4S1.UjNQrZ6Ge4IR6UfvlovLGyhGK', '2021-04-03 05:50:19', '2021-04-03 05:50:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `ID_Carrito` int(11) NOT NULL,
  `ID_Cliente` int(11) NOT NULL,
  `ID_Producto` int(11) NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Costo_Total` float NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Estatus` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `carrito`
--

INSERT INTO `carrito` (`ID_Carrito`, `ID_Cliente`, `ID_Producto`, `Cantidad`, `Costo_Total`, `updated_at`, `created_at`, `Estatus`) VALUES
(1, 1, 10, 2, 700, '2021-04-02 14:14:17', '2021-04-02 14:14:17', 'Pedido'),
(2, 1, 11, 2, 240, '2021-04-03 04:20:33', '2021-04-03 04:20:33', 'Pedido');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `ID_Cliente` int(11) NOT NULL,
  `Nombre` varchar(25) NOT NULL,
  `Ap_Paterno` varchar(25) NOT NULL,
  `Ap_Materno` varchar(25) NOT NULL,
  `Correo` varchar(30) NOT NULL,
  `Contrasenia` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`ID_Cliente`, `Nombre`, `Ap_Paterno`, `Ap_Materno`, `Correo`, `Contrasenia`, `updated_at`, `created_at`) VALUES
(1, 'Adrian', 'Arenas', 'Joaquin', 'alexis@hotmail.com', '$2y$10$qXAagtRYRZXtEAC1KEA.LuR3EM3mE5I85OWoifYvkf76FXDjmwlNu', '2021-04-01 02:55:11', '2021-04-01 02:55:11'),
(2, 'Daniela', 'Calle', 'Arriaga', 'dani@hotmail.com', '$2y$10$Z0o4qwdnVMdgDHl6WThNwecwhNG3pt0pNd7vOHAHw62TRW4PkAhba', '2021-04-03 05:48:27', '2021-04-03 05:48:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `ID_Producto` int(11) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `Cantidad_Inicial` int(11) NOT NULL,
  `Cantidad_Disponible` int(11) NOT NULL,
  `Precio_Compra` float NOT NULL,
  `Precio_Venta` float NOT NULL,
  `Descripcion` varchar(50) NOT NULL,
  `Tipo` varchar(30) NOT NULL,
  `Imagen1` varchar(255) DEFAULT NULL,
  `Imagen2` varchar(255) DEFAULT NULL,
  `ID_AdminAutor` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`ID_Producto`, `Nombre`, `Cantidad_Inicial`, `Cantidad_Disponible`, `Precio_Compra`, `Precio_Venta`, `Descripcion`, `Tipo`, `Imagen1`, `Imagen2`, `ID_AdminAutor`, `updated_at`, `created_at`) VALUES
(10, 'Balon Adidas', 10, 10, 250, 350, 'Balon de soccer adidas', 'Balon', '/images/balon.jpg', '/images/balon2.jpg', 2, '2021-04-02 09:44:56', '2021-04-02 09:44:56'),
(11, 'Conos de colores', 100, 100, 75, 120, 'Conos de deporte de colores', 'Conos', '/images/conosDeporte1.jpg', '/images/conosDeporte2.jpg', 2, '2021-04-03 04:14:44', '2021-04-03 04:14:44'),
(12, 'Cuerdas', 30, 30, 30, 75, 'Cuerdas elasticas', 'Cuerdas', '/images/cuerdasDeporte1.jpg', '/images/cuerdasDeporte2.png', 2, '2021-04-03 04:16:30', '2021-04-03 04:16:30'),
(13, 'Balon Americano Wilson', 100, 100, 250, 350, 'Balon de americano Wilson', 'Balon', '/images/balonAmericano1.jpg', '/images/balonAmericano2.jpg', 2, '2021-04-03 04:18:38', '2021-04-03 04:18:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administradores`
--
ALTER TABLE `administradores`
  ADD PRIMARY KEY (`ID_Admin`),
  ADD UNIQUE KEY `Correo` (`Correo`);

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD PRIMARY KEY (`ID_Carrito`),
  ADD KEY `ID_Cliente` (`ID_Cliente`),
  ADD KEY `ID_Producto` (`ID_Producto`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`ID_Cliente`),
  ADD UNIQUE KEY `Correo` (`Correo`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`ID_Producto`),
  ADD KEY `ID_AdminAutor` (`ID_AdminAutor`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administradores`
--
ALTER TABLE `administradores`
  MODIFY `ID_Admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `carrito`
--
ALTER TABLE `carrito`
  MODIFY `ID_Carrito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `ID_Cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `ID_Producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD CONSTRAINT `carrito_ibfk_1` FOREIGN KEY (`ID_Cliente`) REFERENCES `clientes` (`ID_Cliente`),
  ADD CONSTRAINT `carrito_ibfk_2` FOREIGN KEY (`ID_Producto`) REFERENCES `productos` (`ID_Producto`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`ID_AdminAutor`) REFERENCES `administradores` (`ID_Admin`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
