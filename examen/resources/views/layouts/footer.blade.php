<footer class="footer">
    <div class="container-fluid text-center">
        <div class="copyright text-center">
            &copy; {{ now()->year }} {{ __('Creador por') }}
            <a href="https://www.facebook.com/profile.php?id=100048970758452" target="_blank">{{ __('Alejandro Suarez') }}</a>{{', '}}
            <a href="https://www.facebook.com/AxelAdanGS" target="_blank">{{ __('Axel Adan') }}</a> &amp;
            <a href="https://www.facebook.com/alex.arenas.1829" target="_blank">{{ __('Alexis Arenas') }}</a> {{ __('para un proyecto :C') }}
        </div>
    </div>
</footer>
