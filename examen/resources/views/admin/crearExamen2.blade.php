@extends('layouts.appAdmin')
@section('content')
    <br>
    <div class="card">
        <div class="card-body">
            <h1>Examen {{$examen->titulo}}</h1>

            <form action="{{route('crear.examen.4',$examen->id)}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    @for($i=1;$examen->numeroP>=$i;$i++)
                        <br>
                        <label for="exampleFormControlInput1"><b>Ingresa la pregunta {{$i}} </b></label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" name="pregunta{{$i}}" placeholder="Ingresa la pregunta" required>
                        <br>
                        <div class="card col-md-8">
                            <div class="card-body">
                                @for($j=1;$examen->numeroR>=$j;$j++)
                                    <label for="exampleFormControlInput{{$j}}">Ingresa la respuesta {{$j}}</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput{{$j}}" name="respuesta{{$j.$i}}" placeholder="Ingresa la respuesta" required>
                                @endfor
                            </div>
                        </div>
                        <br>
                        <label for="exampleFormControlInput1">Ingresa la respuesta correcta {{$i}}</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" name="respuestaC{{$i}}" placeholder="Ingresa la respuesta que sea correcta" required>
                        <hr>
                    @endfor
                </div>
                <input type="submit" class="btn btn-primary" value="Crear Examen">
            </form>

        </div>
    </div>
@endsection
