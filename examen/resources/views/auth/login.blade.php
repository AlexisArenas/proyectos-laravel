@extends('layouts.app')
@section('content')
    <br>
    <div class="row justify-content-center">
        <div class="card col-md-6">
            <h5 class="card-header">Login</h5>
            <div class="card-body">
                <form method="post" action="{{route('login.form')}}">
                    {{csrf_field()}}
                    @if(isset($estatus))
                        @if($estatus=="success")
                            <label class="text-success">{{$mensaje}}</label>
                        @elseif($estatus=="error")
                            <label class="text-warning">{{$mensaje}}</label>
                        @endif
                    @endif
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Correo</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" name="correo" aria-describedby="emailHelp" required>
                        <div id="emailHelp" class="form-text">No compartas tu correo con nadie.</div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Contraseña</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" name="contra" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Ingresar</button>
                    <hr>
                    @if(isset($_GET["r"]))
                        <input type="hidden" name="url" value="{{$_GET["r"]}}">
                    @endif
                </form>
            </div>
        </div>
    </div>
@endsection
