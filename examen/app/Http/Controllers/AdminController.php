<?php

namespace App\Http\Controllers;

use App\Models\Examen;
use App\Models\Preguntas;
use App\Models\Respuestas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public  function index(){
        $exams = Examen::all();
        return view('admin.indexAdmin',compact('exams'));
    }

    public function eliminarExamen($id){

        $eliminarE=Examen::find($id);
        $verificar=$eliminarE->delete();

        return redirect(route('admin.index'));
    }
    public function crearExamen(){
        return view('admin.crearExamen1');
    }

    public function crearExamen2(Request $datos){
        if(!$datos->nombre || !$datos->preguntas || !$datos->respuestas)
            return view('admin.crearExamen1',["estatus"=> "error", "mensaje"=> "¡Falta información no se puedo crear el Examen!"]);

        $examen = Examen::where('titulo',$datos->nombre)->first();
        if($examen)
            return view('admin.crearExamen1',["estatus"=> "error", "mensaje"=> "¡El nombre ya se encuentra registrado!"]);

        $nombre = $datos->nombre;
        $preguntas = $datos->preguntas;
        $respuestas = $datos->respuestas;

        $examen = new Examen();
        $examen->titulo =  $nombre;
        $examen->numeroP = $preguntas;
        $examen->numeroR = $respuestas;
        $examen->save();
        return redirect(route('crear.examen.3',$nombre));
    }
    public function crearExamen3($nombre){
        $examen = Examen::where('titulo',$nombre)->first();
        return view('admin.crearExamen2',compact('examen'));
    }
    public function crearExamen4(Request $datos,$id){
        $examen = Examen::where('id',$id)->first();

        for ($i=1;$examen->numeroP>=$i;$i++){
            $idExamen=$examen->id;
            $preguntaN='pregunta'.$i;
            $respuestaN='respuestaC'.$i;
            $pregunta=$datos->$preguntaN;
            $respuestaC=$datos->$respuestaN;
            echo json_encode($datos->$preguntaN) ;
            $preguntas = new Preguntas();
            $preguntas -> id_examen=$idExamen;
            $preguntas->pregunta=$pregunta;
            $preguntas->respuestaC=$respuestaC;
            $preguntas->save();
            for ($j=1;$examen->numeroR>=$j;$j++){
                $idPregunta=Preguntas::where('pregunta',$pregunta)->first();
                $idPregunta2=$idPregunta->id;
                $respuestasN='respuesta'.$j.$i;
                $respuesta=$datos->$respuestasN;
                $respuestaModel= new Respuestas();
                $respuestaModel->id_pregunta=$idPregunta2;
                $respuestaModel->id_examen=$idExamen;
                $respuestaModel->respuesta=$respuesta;
                $respuestaModel->save();
            }

        }
        return redirect(route('admin.index'));
    }
    public function  realizarExamen($id){
        $examen=Examen::where('id',$id)->first();
        $preguntas = DB::table('preguntas')->where('id_examen', $examen->id)->get();
        $respuestas = DB::table('respuestas')->where('id_examen', $examen->id)->get();
        return view('admin.realizarExamen',compact('examen','preguntas','respuestas'));
    }
}
