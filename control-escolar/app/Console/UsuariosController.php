<?php

namespace App\Console;

use App\Http\Controllers\Controller;
use App\Models\Alumnos;
use App\Models\Calificaciones;
use App\Models\Documentos;
use App\Models\Usuario;
use App\Models\Usuarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class UsuariosController extends Controller
{
    public function login(){
        return view("login");
    }

    public function register(){
        return view("register");
    }

    public function home(){
        return view("home");
    }

    public function scoreStudent(){
        return view("scoreStudent");
    }

    public function verifyCredentials(Request $datos){
        if (!$datos->email || !$datos->password) {
            return view('login', ["estatus" => "error", "mensaje" => "Complete los campos"]);
        }

        $user = Usuarios::where("email", $datos->email)->first();
        if (!$user) {
            return view('login', ["estatus" => "error", "mensaje" => "No existe una cuenta vinculada con ese correo electrónico"]);
        }

        if (!Hash::check($datos->password, $user->password))
            return view('login', ["estatus" => "error", "mensaje" => "El correo electrónico y/o contraseña son incorrectos"]);

        Session::put('usuario', $user);

        return redirect()->route('home');
    }

    public function registerForm(Request $datos){
        if(!$datos->email || !$datos->password1 || !$datos->password2)
            return view("register",["estatus"=> "error", "mensaje"=> "¡Falta información!"]);

        $usuario = Usuarios::where('email',$datos->email)->first();
        if($usuario)
            return view("register",["estatus"=> "error", "mensaje"=> "Ya existe una cuenta vinculada con ese correo electronico"]);

        $name = $datos->name;
        $paternalSurname = $datos->paternalSurname;
        $maternalSurname = $datos->maternalSurname;
        $email = $datos->email;
        $password2 = $datos->password2;
        $password1 = $datos->password1;

        if($password1 != $password2){
            return view("register",["estatus" => "Las contraseñas deben coincidir"]);
        }

        $usuario = new Usuarios();
        $usuario->name = $name;
        $usuario->paternalSurname = $paternalSurname;
        $usuario->maternalSurname = $maternalSurname;
        $usuario->email =  $email;
        $usuario->password = bcrypt($password1);
        $usuario->save();

        return view("login",["estatus"=> "success", "mensaje"=> "¡Cuenta creada con exito!"]);
    }

    public  function signOff() {
        if(Session::has('usuario'))
            Session::forget('usuario');

        return redirect()->route('login');
    }

    public function registerDocuments(Request $documents)
    {
        $route = "../documents/";

        $birthCertificate = $route . basename($_FILES["birthCertificate"]["name"]);
        $curp = $route . basename($_FILES["curp"]["name"]);
        $ine = $route . basename($_FILES["ine"]["name"]);

        $typeFileBirthCertificate = strtolower(pathinfo($birthCertificate, PATHINFO_EXTENSION));
        $typeFileCurp = strtolower(pathinfo($curp, PATHINFO_EXTENSION));
        $typeFileIne = strtolower(pathinfo($ine, PATHINFO_EXTENSION));

        $sizeDocumentBirthCertificate = filesize($_FILES["birthCertificate"]["tmp_name"]);
        $sizeDocumentCurp = filesize($_FILES["curp"]["tmp_name"]);
        $sizeDocumentIne = filesize($_FILES["ine"]["tmp_name"]);

        if ($sizeDocumentBirthCertificate != false || $sizeDocumentCurp != false || $sizeDocumentIne != false) {
            $sizeBirthCertificate = $_FILES["birthCertificate"]["size"];
            $sizeCurp = $_FILES["curp"]["size"];
            $sizeIne = $_FILES["ine"]["size"];

            if ($sizeBirthCertificate > 500000 || $sizeCurp > 500000 || $sizeIne > 500000) {
                return view("home", ["estatus" => "error", "mensaje" => "El tamaño de un archivo debe ser menor a 5 Mb"]);
            } else {
                if ((($typeFileBirthCertificate == "png" || $typeFileBirthCertificate == "jpge" || $typeFileBirthCertificate == "pdf")
                        && ($typeFileCurp == "png" || $typeFileCurp == "jpge" || $typeFileCurp == "pdf"))
                    && ($typeFileIne == "png" || $typeFileIne == "jpge" || $typeFileIne == "pdf")) {
                    if (move_uploaded_file($_FILES["birthCertificate"]["tmp_name"], $route)) {
                        return view("home", ["estatus" => "success", "mensaje" => "¡Registro completado!"]);
                    } else {
                        return view("home", ["estatus" => "error", "mensaje" => "¡Ups! Hubo un error al enviar sus documentos. Intentelo de nuevo"]);
                    }
                } else {
                    return view("home", ["estatus" => "error", "mensaje" => "Tipo de archivo no permitido"]);
                }
            }
        }



            /*if ($documents->hasFile('birthCertificate') && $documents->hasFile('curp') && $documents->hasFile('ine')) {
                $fileOne = $documents->file('birthCertificate');
                $fileTwo = $documents->file('curp');
                $fileThree = $documents->file('ine');

                $documentOne = $fileOne->getClientOriginalName();
                $documentTwo = $fileTwo->getClientOriginalName();
                $documentThree = $fileThree->getClientOriginalName();

                $extensionOne = $fileOne->getClientOriginalExtension();
                $extensionTwo = $fileTwo->getClientOriginalExtension();
                $extensionThree = $fileThree->getClientOriginalExtension();

                $fileOne->move(public_path(). '/documents', $extensionOne);
                $fileTwo->move(public_path(). '/documents', $extensionTwo);
                $fileThree->move(public_path(). '/documents', $extensionThree);
            }

            if (!$documents->birthCertificate || !$documents->curp || !$documents->ine)
                return view("home",["estatus"=> "error", "mensaje"=> "Faltan Documentos"]);

            $birthCertificate = $documents->birthCertificate;
            $curp = $documents->curp;
            $ine = $documents->ine;

            $documentsUser = new Documentos();
            $documentsUser->birthCertificate = $birthCertificate;
            $documentsUser->curp = $curp;
            $documentsUser->ine = $ine;
            $documentsUser->save();

            return view("scoreStudent",["estatus"=> "success", "mensaje"=> "¡Registro Terminado! Ahora registre las calificaciones"]);*/

            //Nel
            /*if (!$documents->birthCertificate || !$documents->curp || !$documents->ine)
                    return view('home', ["estatus" => "error", "mensaje" => "¡Faltan Documentos!"]);

                if ($documents->hasFile('birthCertificate') || $documents->hasFile('curp') || $documents->hasFile('ine')) {
                    $fileOne = $documents->file('birthCertificate');
                    $fileTwo = $documents->file('curp');
                    $fileThree = $documents->file('ine');
                }

                $nameOne = $fileOne->getClientOriginalName();
                $nameTwo = $fileTwo->getClientOriginalName();
                $nameThree = $fileThree->getClientOriginalName();

                $extensionOne = $fileOne->getClientOriginalExtension();
                $extensionTwo = $fileTwo->getClientOriginalExtension();
                $extensionThree = $fileThree->getClientOriginalExtension();


                if ($extensionOne != "png" && $extensionOne != "jpge" && $extensionOne != "pdf") {
                    return view('home', ["estatus" => "error", "mensaje" => "La extensión de uno o más archivos no es admitida"]);
                }
                elseif ($extensionTwo != "png" || $extensionTwo != "jpge" || $extensionTwo != "pdf") {
                    return view('home', ["estatus" => "error", "mensaje" => "La extensión de uno o más archivos no es admitida"]);
                }
                elseif ($extensionThree != "png" || $extensionThree != "jpge" || $extensionThree != "pdf") {
                    return view('home', ["estatus" => "error", "mensaje" => "La extensión de uno o más archivos no es admitida"]);
                }

                $documentsUser = Usuarios();
                $documentsUser->birthCertificate = $nameOne;
                $documentsUser->curp = $nameTwo;
                $documentsUser->ine = $nameThree;
                $documentsUser->save();

                return view('home', ["estatus" => "susses", "mensaje" => "¡Registro completado!"]);*/
            /*if(!$documentos->name || !$documentos->paternalSurname || !$documentos->maternalSurname)
                    return view("home",["estatus"=> "error", "mensaje"=> "¡Falta uno o más campos por llenar!"]);

                $name = $documentos->name;
                $paternalSurname = $documentos->paternalSurname;
                $maternalSurname = $documentos->maternalSurname;

                $alumnos = new Alumnos();
                $alumnos->name = $name;
                $alumnos->paternalSurname = $paternalSurname;
                $alumnos->maternalSurname = $maternalSurname;
                $alumnos->save();*/
            /*if ($documents->hasFile('birthCertificate'))
                    $file = $documents->file('birthCertificate');

                $nameDocument = $file->getClientOriginalName();

                $extension = $file->getClientOriginalExtension();
                if ($extension != "png" && $extension != "jpge" && $extension != "pdf")
                    return view("home",["estatus"=> "error", "mensaje"=> "Los documentos deben ser de extensión .png, .jpge ó .pdf"]);

                $file->move(public_path(). '/documents', $nameDocument);

                $birthCertificate = $documents->birthCertificate;
                $route = $nameDocument;

                $documentsUser = Usuario();
                $documentsUser->birthCertificate = $birthCertificate;
                $documentsUser->route = $route;
                $documentsUser->save();

                return view("home",["estatus"=> "success", "mensaje"=> "¡Registro Completado!"]);*/
    }

    public function registerScore(Request $score){
        if(!$score->ingles || !$score->etica || !$score->matematicas || !$score->programacion || !$score->redes  || !$score->arquitectura   || !$score->sistemas)
            return view("scoreStudent",["estatus"=> "error", "mensaje"=> "¡Faltan calificaciones!"]);

        $calificacionIngles = Calificaciones::where('ingles',$score->ingles)->first();
        $calificacionEtica = Calificaciones::where('etica',$score->etica)->first();
        $calificacionMatematicas = Calificaciones::where('matematicas',$score->matematicas)->first();
        $calificacionProgramacion = Calificaciones::where('programacion',$score->programacion)->first();
        $calificacionRedes = Calificaciones::where('redes',$score->redes)->first();
        $calificacionArquitectura = Calificaciones::where('arquitectura',$score->arquitectura)->first();
        $calificacionSistemas = Calificaciones::where('sistemas',$score->sistemas)->first();
        if($calificacionIngles || $calificacionEtica || $calificacionMatematicas || $calificacionProgramacion || $calificacionRedes || $calificacionArquitectura || $calificacionSistemas)
            return view("scoreStudent",["estatus"=> "error", "mensaje"=> "Ya se registrado una o más calificaciones"]);

        $ingles = $score->ingles;
        $etica = $score->etica;
        $matematicas = $score->matematicas;
        $programacion = $score->programacion;
        $redes = $score->redes;
        $arquitectura = $score->arquitectura;
        $sistemas = $score->sistemas;

        $calificaciones = new Calificaciones();
        $calificaciones->ingles = $ingles;
        $calificaciones->etica = $etica;
        $calificaciones->matematicas = $matematicas;
        $calificaciones->programacion =  $programacion;
        $calificaciones->redes =  $redes;
        $calificaciones->arquitectura =  $arquitectura;
        $calificaciones->sistemas =  $sistemas;
        $calificaciones->save();

        return view("scoreStudent",["estatus"=> "success", "mensaje"=> "¡Calificaciones Registradas!"]);
    }

    public function checkScore(){
        // Obtener todas las filas de la tabla
        $id = DB::table('calificaciones')->get();

        foreach ($id as $id) {
            var_dump($id->id);
        }

        /*//obtener id
        $querry = DB::table('calificaciones')->select('id');
        $id = $querry->addSelect('id')->get();
        //$id = DB::table('calificaciones')->where('id', $querry)->get();
        json_decode($id);*/

        /*$id = $datos->id;

        $idCalificacion = new Calificaciones();
        $idCalificacion->id = $id;
        $idCalificacion->get();

        if (!$id)
            return view("checkScore",["estatus"=> "error", "mensaje"=> "Error"]);

        if ($id)
            return view("checkScore",["estatus"=> "susses", "mensaje"=> "Chido"]);

        //$id = session('id');
        //$scoreIngles = DB::table('calificaciones')->find($id);*/

        return view('checkScore', [var_dump($id)]);
    }
}
